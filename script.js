const cards = document.querySelectorAll(".memory-card");
let hasFlippedCard = false;
let firstCard, secondCard;
function flipCard(){
    this.classList.toggle('flip');
    if (!hasFlippedCard) {
      hasFlippedCard = true;
      firstCard = this;        
    }else{
        hasFlippedCard = false;
        secondCard = this;
        if (firstCard.dataset.image===secondCard.dataset.image) {
          
            firstCard.removeEventListener('click',flipCard);
            secondCard.removeEventListener('click',flipCard);
            
        }else{
          setTimeout(() => {
              firstCard.classList.remove('flip');
              secondCard.classList.remove('flip');   
          }, 1500); 
        }
    }
}
(function shiftingImg(){
  cards.forEach(card =>{
    let shift = Math.floor(Math.random() * 20);
    card.style.order = shift;
  });
})();

cards.forEach(card => card.addEventListener('click',flipCard));

